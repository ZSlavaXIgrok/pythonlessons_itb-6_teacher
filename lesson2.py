# print(3 == 3)
'''
if выражение 1:
    блок кода 1
elif выражение 2:
    блок кода 2
else:
    блок кода 3
'''

x = 0
if x:
    print('Переменная х вернула ИСТИНУ')
else:
    print('Переменная х вернула ЛОЖЬ')

if 1:
    print('Выражение истинно')
else:
    print('Выражение ложно')

light = 'red'
if light == 'red':
    print('Stop')
elif light == 'yellow':
    print('Wait')
elif light == 'green':
    print('Go')
else:
    print('What?')


