#### Крестики-нолики. Консольная игра ####

# Функции
def default():
    # Вывод сообщения при старте игры
    print('\nДобро пожаловать! Давай сыграем в игру "Крестики-нолики"\n')


def rules():
    # Вывод правил игры "Крестики-нолики"
    print("Игровое поле имеет следующий вид:\n")
    print("Позиции игрового поля будут похожи на цифры цифровой клавиатуры.\n")
    print("7 | 8 | 9")
    print("---------")
    print("4 | 5 | 6")
    print("---------")
    print("1 | 2 | 3")
    print("\nВыбери позицию от 1 до 9.\n")


# Спрашиваем игрока о готовности к игре
def play():
    return input("\nГотовы ли Вы к игре? [Д]а или [Н]ет.\t").upper().startswith('Д')


# Ввод имён игроков (для режима игрок против игрока)
def names():
    p1_name = input("\nВВЕДИТЕ имя ИГРОКА 1: \t").capitalize()
    p2_name = input("\nВВЕДИТЕ имя ИГРОКА 2: \t").capitalize()
    return p1_name, p2_name


# Выбор чем будет играть игрок (крестиком или ноликом)
def choice():
    p1_choice = " "
    p2_choice = " "
    while p1_choice != 'X' or p1_choice != 'O':
        p1_choice = input("Хотите ли Вы играть крестиком или ноликом? Введите 'X' или 'O'. ")[0].upper()

        # Анализ ввода от игроков
        if p1_choice == 'X' or p1_choice == 'O':
            break  # выходим из цикла
        print("НЕПРАВИЛЬНЫЙ ВВОД. Повторите снова.")

    if p1_choice == 'X':
        p2_choice = 'O'
    elif p1_choice == 'O':
        p2_choice = 'X'
    return p1_choice, p2_choice

# Случайный выбор кто будет ходить первым
def first_player():
    import random
    return random.choice((0, 1))


# Отображение игрового поля
def display_board(board, avail):
    print("    " + " {} | {} | {} ".format(board[7], board[8], board[9]) + "         " + " {} | {} | {} ".format(avail[7], avail[8], avail[9]))
    print("    " + "--------------" + "         " + "--------------")

# ОСНОВНОЙ КОД ИГРЫ
print("\n\t\tКрестики-нолики\n")
input("Нажми ENTER, чтобы начать игру!")
default()
rules()

### Основной игровой цикл ###
while True:
    # Создаём список позиций игрового поля
    theBoard = [" "] * 10
    # Создаём список доступных позиций на игровом поле
    available = [str(num) for num in range(0, 10)]  # Генерация списка
    # available = '0123456789'
    print("\n[0]. Игрок против компьютера.")
    print("[1]. Игрок против игрока.")
    print("[2]. Компьютер против компьютера.")
    mode = int(input("\nВыберите нужный Вам режим игры. Введите [0] - [2]."))
    # Выбран режим "Игрок против компьютера"
    if mode == 0:
        p1_name = input("\nВведите имя игрока, который будет играть против компьютера:\t").capitalize()
        p2_name = "Компьютер"
        p1_choice, p2_choice = choice()
        print(f'\n{p1_name}:', p1_choice)
        print(f'{p2_name}:', p2_choice)
    # Выбран режим "Игрок против Игрока"
    elif mode == 1:
        # Спрашиваем имена игроков
        p1_name, p2_name = names()
        # Спрашиваем кем кто будет играть (X или O)
        p1_choice, p2_choice = choice()
        print(f'\n{p1_name}:', p1_choice)
        print(f'{p2_name}:', p2_choice)
    # Выбран режим "Компьютер против Компьютера"
    else:
        p1_name = "Компьютер1"
        p2_name = "Компьютер2"
        p1_choice, p2_choice = "X", "O"
        print(f'\n{p1_name}:', p1_choice)
        print(f'{p2_name}:', p2_choice)
