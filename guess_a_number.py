# Это игра по угадыванию чисел
import random

# Счётчик количества попыток
guessesTaken = 0

myName = input("Привет! Как тебя зовут?\n")

number = random.randint(1, 20)
print(f'Что ж, {myName} , я загадаю число от 1 до 20.')

for guessesTaken in range(6):
    print('Попробуй угадать.')
    guess = int(input())

    if guess < number:
        print('Твоё число слишком мало.')
    elif guess > number:
        print('Твоё число слишком велико.')
    elif guess == number:
        break

if guess == number:
    guessesTaken = str(guessesTaken + 1)
    print(f'Отлично, {myName} ! Ты справился за {guessesTaken} попытки!')

if guess != number:
    number = str(number)
    print(f'Увы, не отгадал.  Было загадано число {number}')